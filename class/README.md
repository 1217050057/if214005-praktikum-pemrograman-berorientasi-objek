# Class
> Class adalah blueprint / template dari objek representasi permasalahan dunia nyata

## Bahasa Java
### Pendefinisian Class
```java
class BajakLaut {

    /**
    * Pada bahasa Java, ATTRIBUTE memiliki tipe data, contohnya di bawah ini STRING
    * Pada bahasa Java, secara bawaan ATTRIBUTE nya memiliki MODIFIER PUBLIC
    * atau artinya, ATTRIBUTE tersebut dapat diakses oleh CLASS eksternal manapun
    */
    String nama;
    
    /**
    * Modifier PRIVATE digunakan jika ATTRIBUTE tersebut
    * hanya bisa diakses dari dalam CLASS ini
    */
    private int totalHarta;

    /**
    * Modifier PROTECTED digunakan jika ATTRIBUTE tersebut
    * hanya bisa diakses oleh CLASS ini dan turunan dari CLASS ini
    */
    protected String fileBendera;

    /**
    * METHOD dapat mengembalikan nilai dengan tipe data tertentu contohnya INTEGER
    */
    int getTotalHarta() {

        /**
        * Kata kunci this digunakan untuk mengakases ATTRIBUTE atau METHOD dari CLASS ini
        * Tipe data RETURN harus sama dengan tipe data di pendefinisian fungsi
        * contohnya di sini adalah int
        */
        return this.totalHarta;
    }

    /**
    * METHOD OVERLOADING adalah ketika method yang sama memiliki parameter yang berbeda 
    */
    String getTotalHarta(boolean pakeRp) {
        if(pakeRp) {
            return "Rp " + this.totalHarta;
        } else {
            return String.valueOf(this.totalHarta);
        }
    }

    /**
     * METHOD juga dapat tidak mengembalikan nilai atau tipe data returnnya VOID
     */
    void kabur() {
        System.out.println("Kabur");
    }
    /**
    * METHOD dapat memiliki satu atau lebih parameter
    * Di bawah ini contohnya, tambah harta memiliki parameter jumlah
    */
    void tambahHarta(int jumlah) {

        /**
        * A += B artinya A = A + B
        */
        this.totalHarta += jumlah;
    }
}
```

### Instansiasi Object dari Class
```java
/**
* Sintaks new digunakan untuk membuat OBJECT baru
* dari CLASS
*/
BajakLaut bajakLaut = new BajakLaut();
```

## Bahasa TypeScript
### Pendefinisian Class
```ts
class BajakLaut {

    nama: string;

    private totalHarta: number;

    getTotalHarta(): number {
        return this.totalHarta;
    }

    kabur(): void {
        console.log("Kabur");
    }
    
    tambahHarta(jumlah: number): void {
        this.totalHarta += jumlah;
    }
}
`````

## Dart
> coming soon

## Python
> coming soon
